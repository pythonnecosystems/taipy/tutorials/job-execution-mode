# 작업 실행 모드(Job Execution mode)

코드는 [여기서](https://docs.taipy.io/job_execution.py) 다운을 수 있다. 여기 [TOML 파일](https://docs.taipy.io/config.toml)이 있는 [Python 포로그램](https://docs.taipy.io/job_execution_toml.py)이다.

## 작업 실행
Taipy에는 코드를 실행하는 [다른 방법](https://docs.taipy.io/en/release-3.0/manuals/core/config/job-config/)이 있다. 실행 모드를 변경하면 여러 작업을 병렬로 실행하는 데 유용할 수 있다.

- *standalone* 모드: 비동기. 실행 그래프에 따라 작업을 병렬로 실행할 수 있다(*max_nb_of_workers* > 1인 경우).
- *development* 모드: 동기식. 기본 실행 모드는 *development*이다.

두 가지 실행 모드를 보여주기 위한 구성과 기능을 정의한다.

```python
# Normal function used by Taipy
def double(nb):
    return nb * 2

def add(nb):
    print("Wait 10 seconds in add function")
    time.sleep(10)
    return nb + 10
```

![](./config.svg)

아래 코드는 실행 모드를 변경한다. `standalone`으로 설정하면 Taipy Core가 비동기식으로 작동한다. 이 구성에서 최대 두 개의 작업이 동시에 실행될 수 있다.

```python
Config.configure_job_executions(mode="standalone", max_nb_of_workers=2)

if __name__=="__main__":
    tp.Core().run()
    scenario_1 = tp.create_scenario(scenario_cfg)
    scenario_2 = tp.create_scenario(scenario_cfg)

    scenario_1.submit()
    scenario_2.submit()

    time.sleep(30)
```

두 제출된 작업이 동시에 실행되고 있다. `max_nb_of_workers`가 더 크다면 여러 시나리오를 동시에 실행하고 시나리오의 여러 작업을 동시에 실행할 수 있다.

`submit` 함수에는 몇 가지 옵션이 있다.

- `wait`: `wait`가 `True`이면 submission은 모든 작업이 끝날 때까지 기다린다(*타임아웃*이 정의되지 않은 경우).
- `timeout`: `wait`이 `True`인 경우 Taipy는 일정 시간까지 submission이 끝날 때까지 기다린다.

```python
if __name__=="__main__":
    tp.Core().run()
    scenario_1 = tp.create_scenario(scenario_cfg)
    scenario_1.submit(wait=True)
    scenario_1.submit(wait=True, timeout=5)
```

## 전체 코드

```python
from taipy.core.config import Config
import taipy as tp
import datetime as dt
import pandas as pd
import time

# Normal function used by Taipy
def double(nb):
    return nb * 2

def add(nb):
    print("Wait 10 seconds in add function")
    time.sleep(10)
    return nb + 10

Config.configure_job_executions(mode="standalone", max_nb_of_workers=2)

# Configuration of Data Nodes
input_cfg = Config.configure_data_node("input", default_data=21)
intermediate_cfg = Config.configure_data_node("intermediate", default_data=21)
output_cfg = Config.configure_data_node("output")

# Configuration of tasks
first_task_cfg = Config.configure_task("double",
                                       double,
                                       input_cfg,
                                       intermediate_cfg)

second_task_cfg = Config.configure_task("add",
                                        add,
                                        intermediate_cfg,
                                        output_cfg)

# Configuration of scenario
scenario_cfg = Config.configure_scenario(id="my_scenario",
                                         task_configs=[first_task_cfg,
                                                       second_task_cfg])

Config.export("config_07.toml")

if __name__=="__main__":
    tp.Core().run()
    scenario_1 = tp.create_scenario(scenario_cfg)
    scenario_2 = tp.create_scenario(scenario_cfg)
    scenario_1.submit()
    scenario_2.submit()

    scenario_1 = tp.create_scenario(scenario_cfg)
    scenario_1.submit(wait=True)
    scenario_1.submit(wait=True, timeout=5)
```
